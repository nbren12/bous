* Algorithm

This is a python package for solving the primitive equations with a
rigid lid in the x-z plane. While there are many PDE solving libraries
available, none quite suit atmospheric problems. Most of the available
finite volume libraries (e.g. FiPy) do not implement high resolution
methods shock capturing methods. On the other hand, PyClaw does
implement high resolution methods, but it does not implement the solvers
necessary [@leveque\_finite\_2002].

The governing equations are written as

\begin{align}
  u_t + (uu)_x + (uw)_z &= -p_x\\
                   - b  &= -p_z\\
  b_t + (bu)_x + (bw)_z + N^2 w &= S_b\\
  u_x +w_z &=  0.
\end{align}
A rigid lid is used so that $w|_{z=0,H}=0$.

** Algorithm

After an extensive search, it seem that the MITgcm has the most similar
setup. The algorithm we use is very similar to the one described in
their
[[http://mitgcm.org/public/r2_manual/latest/online_documents/node33.html][user
manual]]\_.

1. Diagnostics: Given $u^n$ calculate $w^n = 0 - \int_0^z u_x dz'$. This
   makes use of the rigid lid assumption. Given $b^n$ calculate the
   hydrostatic portion of the pressure as, $b^n_h = \int_0^z b dz'$.
2. Advection step: $u^* = u^n + \Delta t F^n$,
   $b^{n+1} = b^n + \Delta t B^n$,
3. Projection step: $u^{n+1} = u^* - \Delta t (b_s)_x$, where the
   dynamic barotropic pressure $b_s$ is found by solving the following
   elliptic equation:

   $$\Delta t \partial_{xx} b_s = \partial_x \int_0^H u^*dz.$$

   This is trivially accomplished because it is a 1D set of equations.
   It simply amounts to,

   $$u^{n+1} = u^* - \frac{1}{H}\int_0^H u^*dz$$

* Grid

Staggered grid where velocity is given at $u_{i+1/2}$ and scalars at $u_{i}$ in
both directions. Bottom is $u_{0+1/2}$ and top is $u_{(n+1) +1/2}$.

The last non-ghost cell on top has  w=0.0.  the first ghost cell on bottom has w=0.0

* Scalar and momentum advection

Upwind weno interpolation for scalars. 
velocity interpolated symmetrically to neighboring grid.

** Multicloud model moisture budget



* Pressure solve

$u^+ = u^- - <\tilde{u}>$, where $<\cdot>$ is the vertical averaging operator.
