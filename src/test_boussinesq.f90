program test_boussinesq
  use state_mod, only: dp 
  use scalar_advection_mod, only: weno5_2d_advection
  use momentum_advection_mod, only: u_momentum_advection_2d
  use diagnostics_mod
  implicit none

  integer, parameter :: g=5, nx=200, nz=32, nxg = nx+2*g, nzg = nz+2*g
  real(dp), parameter :: km=1000_dp, hr=3600_dp, ler=1500*km, c=50.0, te=ler/c, N2=0.0001, &
       HT= 16*km, dx = 40*km, dz0=HT/nz

  ! State arrays
  real(dp), dimension(-g+1:nx+g, -g+1:nz+g) :: ux, uz, b

  ! Grid
  real(dp) :: dz(-g+1:nz+g)
  real(dp), dimension(-g+1:nx+g, -g+1:nz+g) :: x, z

  ! Vertical boundary conditions
  real(dp) bbc(2), ubc(2)

  ! Work arrays
  real(dp), dimension(nxg, nzg) :: ux1, uz1, b1, b2
  real(dp), dimension(nxg, nzg) :: f,df,sx,sz,w

  ! Time stepping
  real(dp) :: t, dt, tend

  integer i,j

  dz = dz0
  !! Initialize 
  do j=-g+1,nz+g
     do i=1,nx
        x(i, j) = (i-1) * dx
        z(i, j) = (j-1) * dz(j)

     end do
  end do


  print *, 'dx',dx/km, 'km'

  ! diagnose the vertical velocity
  call diagnose_w(ux, dx, dz, g, uz)

  ! boundary conditions for bouyancy
  bbc(1) = 0.0d0
  bbc(2) = N2*HT

  ubc = (/-50, 50/)

  b = z/z(1,nz) * bbc(2)

  ! Sheared case
  ! ux = 10 * dsqrt(2.0_dp) * dsin(z * 3.14/HT ) &
  !      + (-10)* dsqrt(2.0_dp) * dsin(2*z *3.14/HT )
  ux = 0.0_dp
  uz = 0_dp

  b =   b+ .1*dexp(-((x-nx/2*dx)/(300*km))**2 -  ( (z-8000)/(3000) )**2)

  ! Setup time stepping
  t = 0.0_dp
  ! tend = 0_dp
  tend = 6 * hr
  dt = dx / 4.0 / 50

  write(1) b(1:nx, 1:nz)
  !!! Begin time stepping
  do
     print *, t

     !! BEGIN CODE

     ! Copy into new arrays
     ux1 = ux
     b1  = b

     call boussinesq_hsb_2d_step(ux, uz, b, dx, dz, dt, g, f,&
          df, sx, sz, w, b2)
     call boussinesq_hsb_2d_step(ux, uz, b, dx, dz, dt, g, f,&
          df, sx, sz, w, b2)

     ux = (ux + ux1) /2.0d0
     b  = (b + b1)/2_dp

     !! END CODE

     t = t + dt
     if (t > tend) then
        exit
     end if

 end do !!! End time stepping


 write(2) b(1:nx, 1:nz)

 open(unit=112, file='z.txt')
 write(112, *)  z(g+1,1:nz)
 close(112)


 open(unit=112, file='x.txt')
 write(112, *)  x(1:nx,g+1)
 close(112)
contains

  subroutine boussinesq_hsb_2d_step(ux, uz, b, dx, dz, dt, g, f, &
                df, sx, sz, w, b1)

     real(dp), dimension(:,:) :: ux, uz, b, f
     real(dp) dx, dz(:), dt
     integer g

     ! Work arrays
     real(dp), dimension(:,:) :: sx, sz, w, df, b1

     ! Fill boundary conditions
     call fill_bc_z_dirichlet_scalar(b, bbc, g)
     ! call fill_bc_z_dirichlet_scalar(ux, ubc, g)

     ! call fill_bc_z_neumann_scalar(b, g)
     call fill_bc_z_neumann_scalar(ux, g)

     call fill_bc_z_rigidw(uz, g)

     call fill_bc_x_periodic(ux, g)
     call fill_bc_x_periodic(uz, g)
     call fill_bc_x_periodic(b, g)


     b1 = b ! copy for mom advection

     ! advect bouyancy
     call weno5_2d_advection(ux, uz, b, f, dx, dz, g, df)
     b = b + dt * f

     ! advect velocity
     call u_momentum_advection_2d(ux, uz, b1, f, dx, dz, g, sx, sz, w)
     ux = ux + dt*f

     ! Pressure correction
     call pressure_correct_hsb_2d(ux, dz, g)

     ! diagnose the vertical velocity
     call diagnose_w(ux, dx, dz, g, uz)




     !! END CODE
  end subroutine


end program test_boussinesq
