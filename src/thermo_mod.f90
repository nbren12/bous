module thermo_mod
  use state_mod, only: dp
  implicit none

  real(dp), parameter :: rd = 287.1_dp, &
       cpd=1004_dp,&
       gr=9.81_dp



contains

  function tvirt(T, q) result(y)
    real(dp) T, q
    real(dp) y
    y = (1_dp + 0.61_dp * q) * T
  end function tvirt

  function cp_moist(q) result(y)
    real(dp) q
    real(dp) y
    y = (1_dp + 0.87_dp * q) * cpd
  end function cp_moist

  elemental function temperature_dry (s, p) result(t)
    real(dp) :: t
    real(dp), intent(in) :: s !< entropy,a0
    real(dp), intent(in) :: p !< pressure

    t = 298.15 + dexp((s - 6864.8_dp)/cpd)*(p/1d5)**(rd/cpd)
  end function temperature_dry

  elemental function vol_dry (theta, p) result(alpha)
    real(dp) :: alpha
    real(dp), intent(in) :: theta !< entropy,a0
    real(dp), intent(in) :: p !< pressure

    real(dp) :: t

    t = theta * (p/1d5)**(rd/cpd)
    alpha = t * rd / p
  end function vol_dry

  elemental function entropy_dry (T, p) result(s)
    real(dp) :: s
    real(dp), intent(in) :: T !< temperature
    real(dp), intent(in) :: p !< pressure

    s= 6864.8_dp+ cpd * dlog(T/298.15) + rd * dlog(p/1d5)
  end function entropy_dry

  elemental function theta (T, p) 
    real(dp) :: theta
    real(dp), intent(in) :: T !< temperature
    real(dp), intent(in) :: p !< pressure

    theta= T * (1d5/p)**(rd/cpd)
  end function theta

  !> Calculate base profiles of specific volume and pressure given temperature
  !! Uses the following formulas
  !! rho = p/RT, p0_z = -g*rho, dlog(p)/dz  = -g/RT,
  !! p = p(0) exp(int -g /RT dz)
  !!
  !! The surface pressure is assumed to be 1000 hPa.
  subroutine diagnose_base_profiles (T0, a0, p0, dz, g)
    integer g
    real(dp), intent(in) :: T0(-g+1:) !< T0
    real(dp), intent(in) :: dz
    real(dp), intent(out) :: a0(-g+1:), p0(-g+1:)

    integer i

    ! trapezoid rule
    p0(1) = 0_dp
    do i=2,ubound(T0, 1)
       p0(i) = p0(i-1) - gr/rd * (1_dp/T0(i-1)+1_dp/T0(i)) * dz/2_dp
    end do

    ! actual pressure
    p0 = 1d5 * dexp(p0)

    ! use ideal gas law to get a0
    a0 = rd * T0 / p0

  end subroutine diagnose_base_profiles

end module thermo_mod
