from mako.template import Template

mytemplate  = Template(filename="io_mod.f90")


translations = dict(name="out.nc",
                    variables={
                        'temp': {
                            'name': 'temperature',
                            'units': 'kelvin'},
                        'u': {
                            'name': 'horizontal velocity',
                            'units': 'm s^-1'},
                        'w': {
                            'name': 'vertical velocity',
                            'units': 'm s^-1'}},
                    coords={
                        'z': {
                            'name': 'height',
                            'units': 'm'},
                        'x': {
                            'name': 'horizontal',
                            'units': 'm'},
                        'rec': {
                            'name': 'time',
                            'units': 'seconds'
                        }})

print(mytemplate.render(**translations))
