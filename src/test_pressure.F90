! Set to MMS_TEST 1 to test the dry dynamical core using MMS
! #define MMS_TEST 1
#define MC_ON 1
program test_anelastic
  use state_mod
  use scalar_advection_mod, only: weno5_2d_advection
  use momentum_advection_mod, only: u_momentum_advection_2d=> u_momentum_advection_2d_lf
  use diagnostics_mod
  use physics_mod
  use thermo_mod
  implicit none

  integer rank
  type(state_t) :: grid(1)



  real(dp), parameter :: km=1000_dp, hr=3600_dp, ler=1500*km, c=50.0, te=ler/c, N2=0.0001, &
       HT= 16*km, dx = 160*km

  ! vertical profiles
  real(dp), dimension(nz) :: s0,p0


  ! Vertical boundary conditions
  real(dp) sbc(2), ubc(2)

  ! Work arrays
  real(dp), dimension(nxg, nzg) :: f,df,sx,sz,w
  real(dp) :: rr
  real(dp) :: umid, ubot

  ! Output
  integer, parameter :: snapshot_id = 19
  integer, parameter :: nf = 2


  ! Time stepping
  real(dp) :: t, dt, tend
  real(dp) :: snapshot_dt, gauge_dt, dout
  integer snapshot_count, gauge_count

  integer i,j

  ! Namelist
  namelist /tcontrol/ dt, tend, dout

  ! Rank of current processor
  rank = 1

  !
  ! Initialization
  !
  !! initialize grid and profiles
  grid(rank)%dz = 0

  open(unit=41, file='t0.txt')
  open(unit=42, file='p0.txt')
  open(unit=43, file='pdiff.txt')

  read(41,*) s0(1:nz)
  read(42,*) p0(1:nz)
  read(43,*) grid(rank)%dz(1:nz)


  close(41)
  close(42)
  close(43)

  !! Initialize
  do j=1,nz
     do i=1,nx
        grid(rank)%x(i, j) = ( i-1 ) * dx

        grid(rank)%ux(i,j) = 0.0d0
        grid(rank)%uz(i,j) = 0.0_dp

     end do
  end do


  print *, 'Initial profile information:'
  print *, '----------------------------'
  print *, 'pressure', p0(1:nz)
  print *, 'pot temp', s0(1:nz)
  print *, 'dx',dx/km, 'km'

  !! Initialize parameterizations
  call init_modes(s0, p0, grid(rank)%dz, nz, g)
  call init_params
  !! Intial conditions


  !!! Temperature

  ! gaussian peturbation

  umid = 10d0
  ubot = 0d0

  grid(rank)%ux = umid
  do j=1,nz
     do i=1,nx
        ! grid(rank)%s(i,j) = s0(j)

        ! grid(rank)%s(i,j) = s0(j) - &
        !       10*dexp(-((grid(rank)%x(i,j)-nx/2*dx)/(100*km))**2 &
        !                -((p0(j)-800d2)/(100d2) )**2)
        ! grid(rank)%s(i,j) = s0(j)

        ! grid(rank)%s(i,j) = s0(j) + &

        !      5*dsin(6.3 *grid(rank)%x(i,j)*5/40000 / km) &
        !      *dexp(-((p0(j)-800d2)/(100d2) )**2)

        ! grid(rank)%ux(i,j) = -1*dcos(6.3 *(grid(rank)%x(i,j)*5/40000/ km)) &
        !      *dexp(-((p0(j)-800d2)/(100d2) )**2)

        ! grid(rank)%teb(i) = 0d0 + &
        !      2*dexp(-((grid(rank)%x(i,j)-nx/2*dx)/(80*km))**2)

        ! shear profile
        ! rr = (1000d2-p0(j))/ 300d2
        ! if (p0(j) >= 700d2) grid(rank)%ux(i,j) = (1.0d0-rr) * ubot   + rr * umid

        ! if (dabs(grid(rank)%x(i,j) - nx*dx/2) > 600 * km ) grid(rank)%s(i,j) = s0(j)
        ! if (dabs(p0(j) - 600d2) > 200d2 ) grid(rank)%s(i,j) = s0(j)
        ! grid(rank)%s(i,j) = s0(j) 


        call random_number(rr)
        grid(rank)%s(i,j) = s0(j) + (rr-.5) * .1

        ! grid(rank)%teb(i) = 2d0
        ! grid(rank)%moist_cwv(i) = 2d0

        ! grid(rank)%s(i,j) = grid(rank)%s(i,j)*(1+ rr*.001)
             ! * (dtanh((grid(rank)%x(i,j)-nx/2*dx)/200 * km)+1d0)/2d0

        ! if (dabs(grid(rank)%x(i,j) - nx*dx/2) > 600 * km ) grid(rank)%s(i,j) = s0(j)

     end do
  end do
  !!! Velocity
  call pressure_correct_hsb_2d(grid(rank)%ux, grid(rank)%dz, g) ! Need shear profile to conserve mass

  grid(rank)%p(1:nx,1:nz) = spread(p0, 1, nx)
  grid(rank)%hs = 0d0
  grid(rank)%hc = 0d0
  grid(rank)%moist_cwv = 0d0
  grid(rank)%teb = 0d0




  ! Periodic boundary for grid
  call fill_bc_x_periodic(grid(rank)%p, g)
  call fill_bc_x_periodic(grid(rank)%x, g)

  ! Theta

#ifdef MMS_TEST
     call  mms(x(1:nx,1:nz), p(1:nx,1:nz), 0.0d0, ux, s, ux_tend, th_tend)
#endif


  !
  ! End initialization
  !

  
  !! Setup time stepping
  open(45, file='input.nml', status='old')
  read(45, NML=TCONTROL)
  close(45)

  t = 0.0_dp
  tend = 3600d0 * tend
  snapshot_dt = 60d0 * dout
  gauge_dt = 60d0


  ! open(45, file='input.nml', status='new')
  ! write(45, NML=TCONTROL)
  ! close(45)



  ! diagnose the vertical velocity
  call diagnose_w(grid(rank)%ux, dx, grid(rank)%dz, g, grid(rank)%uz)

  !
  ! Setup output file
  !

  ! Open output file and write information about the grid
  OPEN(UNIT=snapshot_id, FILE = 'snap_shots',&
       FORM = 'unformatted', access='stream', status="unknown")

  write(snapshot_id) nx, nz, grid(rank)%x(1:nx,g+1), p0(1:nz), s0(1:nz)

  ! output initial data
  write(snapshot_id) t, grid(rank)%ux(1:nx,1:nz),&
       grid(rank)%s(1:nx,1:nz), grid(rank)%u_modes(1:nx,:),&
       grid(rank)%th_modes(1:nx,:), grid(rank)%hd(1:nx),&
       th_tend(1:nx,1:nz), qheating(1:nx,1:nz), &
       grid(rank)%teb(1:nx), grid(rank)%moist_cwv(1:nx)
  ! write(snapshot_id) t, grid(rank)%ux(1:nx,1:nz), grid(rank)%s(1:nx,1:nz)
  snapshot_count = 1
  gauge_count= 1

  !!! Begin time stepping
  do

     !! BEGIN CODE

     !
     ! (1) Dynamical Core
     !
     grid(rank)%ux1 = grid(rank)%ux
     grid(rank)%s1 = grid(rank)%s

     call grid(rank)%fill_ghost_cells
     call anelastic_hsb_2d_step(grid(rank)%ux, grid(rank)%uz, grid(rank)%s,&
          dx, grid(rank)%dz, dt, g, f, &
          df, sx, sz, w, grid(rank)%b,&
          grid(rank)%x, grid(rank)%p, t)


     call grid(rank)%fill_ghost_cells
     call anelastic_hsb_2d_step(grid(rank)%ux, grid(rank)%uz, grid(rank)%s,&
          dx, grid(rank)%dz, dt, g, f, &
          df, sx, sz, w, grid(rank)%b,&
          grid(rank)%x, grid(rank)%p, t)


     grid(rank)%ux = (grid(rank)%ux1+ grid(rank)%ux)/2.0d0
     grid(rank)%s =  (grid(rank)%s1+ grid(rank)%s)/2.0d0


#ifdef MC_ON

     grid(rank)%ux1 = grid(rank)%ux
     grid(rank)%s1 = grid(rank)%s


     call grid(rank)%fill_ghost_cells
     call physics_tend(grid(rank)%ux, grid(rank)%s, &
          grid(rank)%teb, grid(rank)%moist_cwv,&
          grid(rank)%hc, grid(rank)%hs, grid(rank)%hd,&
          grid(rank)%teb_st,&
          dx, grid(rank)%dz,&
          grid(rank)%u_modes, grid(rank)%th_modes,&
          ux_tend, th_tend,&
          teb_tend, q_tend,&
          hc_tend, hs_tend,qheating,g)

     call grid(rank)%fill_ghost_cells
     call physics_tend(grid(rank)%ux + dt*ux_tend/2d0,&
          grid(rank)%s + dt*th_tend/2d0, &
          grid(rank)%teb + dt*teb_tend/2d0,&
          grid(rank)%moist_cwv + dt*q_tend/2d0,&
          grid(rank)%hc + dt*hc_tend/2d0,&
          grid(rank)%hs + dt*hs_tend/2d0,&
          grid(rank)%hd,&
          grid(rank)%teb_st,&
          dx, grid(rank)%dz,&
          grid(rank)%u_modes, grid(rank)%th_modes,&
          ux_tend, th_tend,&
          teb_tend, q_tend,&
          hc_tend, hs_tend,qheating,g)

     grid(rank)%ux         =  grid(rank)%ux + dt*ux_tend
     grid(rank)%s          =  grid(rank)%s  + dt*th_tend
     grid(rank)%teb        =  grid(rank)%teb+ dt*teb_tend
     grid(rank)%moist_cwv  =  grid(rank)%moist_cwv + dt*q_tend
     grid(rank)%hc         =  grid(rank)%hc + dt*hc_tend
     grid(rank)%hs         =  grid(rank)%hs+ dt*hs_tend



#endif
     !
     ! (2) Forcing terms
     !

     !! END CODE

     t = t + dt


     ! (3) Output and diagnostics
     !
     if (snapshot_dt * snapshot_count < t) then

        print *, 't=', t/3600, 'hrs  .....   Writing snaphot...'
        write(snapshot_id) t, grid(rank)%ux(1:nx,1:nz),&
             grid(rank)%s(1:nx,1:nz), grid(rank)%u_modes(1:nx,:),&
             grid(rank)%th_modes(1:nx,:), grid(rank)%hd(1:nx),&
             th_tend(1:nx,1:nz), qheating(1:nx,1:nz), &
             grid(rank)%teb(1:nx), grid(rank)%moist_cwv(1:nx)

        snapshot_count = snapshot_count + 1

     end if

     ! high frequency gauge output
     if (gauge_dt * gauge_count < t) then

        write(12, *) t, sum((grid(rank)%ux(:,10))**2)
        gauge_count = gauge_count + 1
     end if


     if (t > tend) then
        exit
     end if

 end do !!! End time stepping


 ! write(2) temperature_dry(s(1:nx, 1:nz), spread(p0, 1, nx))
 ! write(snapshot_id) time, ux, s
 ! write(snapshot_id) t, ux, s

 ! open(unit=112, file='levs.txt')
 ! write(112, *)  p0(1:nz)
 ! close(112)


 ! open(unit=112, file='x.txt')
 ! write(112, *)  x(1:nx,g+1)
 ! close(112)

contains

  subroutine anelastic_hsb_2d_step(ux, uz, s,&
       dx, dz, dt, g, f, &
       df, sx, sz, w, b,&
       x,p, t)

    integer g, nx,nz
     real(dp), intent(inout), dimension(:,-g+1:) :: ux, uz, s, f,x,p
     real(dp), dimension(-g+1:) :: dz !< base state
     real(dp) dx, dt, t


     ! Work arrays
     real(dp), dimension(:,-g+1:) :: sx, sz, w, df, b
     real(dp) bbc(2)

     bbc = (/0_dp, 0_dp/)

     nx = size(s,1)
     nz = size(s,2)

     ! diagnose bouyancy
     b = -vol_dry(s,p)

     ! advection of entropy
     f= 0d0
     call weno5_2d_advection(ux, uz, s, f, dx, dz, g, df)
     s = s + dt * f

     ! sponge layer
     
     ! advect velocity
     f = 0d0
     call u_momentum_advection_2d(ux, uz, b, f, dx, dz, g, sx, sz, w)
     ux = ux + dt*f



#ifdef MMS_TEST
     ! call  mms(x, p, ux, s, u_tend, th_tend)
     call  mms(x, p, t, ux, s, sx, sz)
     ux(1:nx, 1:nz) = ux(1:nx, 1:nz) + dt*sx
     s(1:nx, 1:nz)  = s(1:nx, 1:nz) + dt*sz
#else
     ! ! sponge layer
     call sponge_layer(ux, s, s0, g)
#endif

     ! Pressure correction
     call pressure_correct_hsb_2d(ux, dz, g)

     ! print *, matmul(ux,dz)

     ! diagnose the pressure velocity
     call diagnose_w(ux, dx, dz, g, uz)

     !! END CODE
   end subroutine anelastic_hsb_2d_step

   subroutine sponge_layer (ux, s, s0, g)
     integer g
     real(dp), dimension(-g+1:,-g+1:), intent(inout) :: ux, s
     real(dp) s0(:)

     ! Work
     ! Sponge layer
     integer, parameter :: nsponge = 4
     real(dp) zsponge(nsponge)

     integer i


     zsponge = (/((nsponge+1-i)**2, i=1,nsponge)/) / (5* hr )

     do i=1,nsponge
        ! relax velocity
        ux(:,i) = ux(:, i) * dexp(-zsponge(i)*dt)

        ! relax pottemp
        s(:,i) = (s(:, i) - s0(i)) * dexp(-zsponge(i)*dt) &
             + s0(i)
     end do


   end subroutine sponge_layer

#ifdef MMS_TEST
   elemental subroutine mms(x,p, t, u0, th0, u_ten, th_ten)
     real(8), intent(in) :: x, p, t
     real(8), intent(out) :: u0, th0, u_ten, th_ten
     include 'inc.f90'
   end subroutine mms
#endif

 end program test_anelastic
