module diagnostics_mod
  use state_mod, only: dp
  implicit none

contains


!> Perform the surface pressure correction for 2d anelastic flow
!! @param ux(:,nz+2*g) the horizontal velocity
!! @param a0(nz) base state specific volume profile
!! @param g number of ghost cells
  subroutine pressure_correct_hsb_2d(ux, dz, g)
    real(dp), intent(inout) :: ux(:,:)
    real(dp), intent(in) :: dz(:)
    integer g

    real(dp) mu, m0
    integer i, n
    n = size(ux,2)

    ! mass of column
    m0 = sum(dz(g+1:n-g))

    do i=1,size(ux,1)
       mu = sum(ux(i,g+1:n-g) * dz(g+1:n-g)) / m0
       ux(i,:) =ux(i,:) - mu
    end do

  end subroutine pressure_correct_hsb_2d

  !> Diagnose the vertical velocity using mass conservation
  !! @param ux(:,nz+2*g) the horizontal velocity
  !! @param a0(nz) base state specific volume profile
  subroutine diagnose_w(ux, dx, dz, g, uz)
    ! u(:,1:g) = 0.0
    real(dp) :: ux(:,:), dx, dz(:)
    real(dp), intent(out) :: uz(:, :) !< mass weighted vertical velocity rho_0 w
    integer i,j, g

    ! Start integration at 0.0d0 (from rigid lid)
    do i=1,size(ux,1)
       uz(i, g) =  0_dp
    end do

    do j=g+1,size(ux,2)
       do i=2,size(ux,1)
          uz(i, j) = uz(i,j-1) - (ux(i,j)-ux(i-1,j)) * dz(j)/dx
          ! uz(i, j) = uz(i,j-1) - (-ux(i+1,j)+8d0*ux(i,j)-8d0*ux(i-1,j) + ux(i-2,j)) * dz(j)/dx/12d0
       end do
    end do

  end subroutine diagnose_w


end module diagnostics_mod
