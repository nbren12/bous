from pylab import *
from mpl_toolkits.axes_grid1 import AxesGrid
from scipy.io import FortranFile
import xray
import os

x  = loadtxt("x.txt")/1000
z  = loadtxt("z.txt")/1000

nx = len(x)
nz = len(z)


def load_fort(fname, t):

    arr = FortranFile(fname).\
            read_reals(dtype=np.float64).\
            reshape((nx,nz), order='F')

    arr -= arr[0,:]
    ds = xray.Dataset({'b': (['lev', 'x'], arr.T )},
                        coords={'lev':z,
                                'x':x,
                                'time':t})
    ds.lev.attrs['units'] = 'km'
    ds.b.attrs['units'] = 'm s^-2'
    ds.x.attrs['units'] = 'km'
 
    ds.time.attrs['units'] = 'hours'

    return ds




xray.concat((load_fort("fort.1", 0),
             load_fort("fort.2", 6),),
            'time')\
            .to_netcdf("out.nc")

os.system("ncl boussinesq_tend.ncl")

