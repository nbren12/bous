import numpy as np
import matplotlib.pyplot as plt
from numpy import genfromtxt
from mpl_toolkits.axes_grid1 import AxesGrid
from scipy.io import FortranFile
import xray
import os

fields = [('ux', ['time', 'pressure', 'x']),
          ('theta', ['time', 'pressure', 'x']),
          ('u_modes', ['time', 'uind', 'x'],),
          ('th_modes', ['time', 'thind', 'x']),
          ('hd', ['time','x']),
          ('th_tend', ['time','pressure', 'x']),
          ('qheating', ['time','pressure', 'x']),
          ('teb', ['time', 'x']),
          ('moist', ['time', 'x'])]
dims = {}
dims['time'] = [0]
dims['uind'] = [1,2]
dims['thind'] = [1,2]

def load_fort(fname):
    f = open(fname, 'rb')
    nx, nz = np.fromfile(f, np.int32, count=2)

    x = np.fromfile(f, np.float64, count=nx)
    p = np.fromfile(f, np.float64, count=nz)
    th0 = np.fromfile(f, np.float64, count=nz)

    dims['pressure'] = p
    dims['x'] = x


    def get_time_snapshot():
        while True:
            t  = np.fromfile(f, np.float64, count=1)
            if len(t) == 0:
                break

            outd = {}
            for name, dim_spec in fields:
                sh = [len(dims[nm]) for nm in dim_spec]
                outd[name] = np.fromfile(f, np.float64, count=np.prod(sh))\
                               .reshape(sh)

            yield t, outd


    snapshot_iter = get_time_snapshot()

    # Load first sample
    t, data = next(snapshot_iter)

    times = [t]
    for key in data:
        data[key] = [data[key]]

    # Process all snapshots
    for t, snap in  snapshot_iter:
        times.append(t)
        for key in snap:
            data[key].append(snap[key])


    xray_data = {name:(dimspec, np.concatenate(data[name],axis=0))
                 for name, dimspec  in fields}
    xray_data['th0'] = (['pressure', ], th0)

    dims['time'] = np.concatenate(times)
    
    ds = xray.Dataset(xray_data, coords=dims)

    ds.pressure.attrs['units'] = 'Pa'
    ds.theta.attrs['units'] = 'K'
    ds.x.attrs['units'] = 'm'
    ds.time.attrs['units'] = 'seconds since 1900-1-1 0:0:0'
    ds.time.attrs['long_name'] = "time"

    return ds


rule report:
    input: "plot.png", "th.pdf"

rule run:
    input: "test_pressure"
    output: "snap_shots"
    shell:
        "rm -f snap_shots && ./test_pressure"


rule pp:
    input: "snap_shots"
    output: "out.nc"
    run:
        ds = load_fort('snap_shots').to_netcdf("out.nc")

rule anom:
    input: "out.nc"
    output: "anom.nc"
    run:
        import xray
        ds = xray.open_dataset(input[0])
        ds['pert'] = ds['theta'] - ds['th0']

        ds.to_netcdf(output[0])


rule plot:
    input: "anom.nc"
    output: "th.pdf"
    shell:
        "ncl ./pressure_tend.ncl"

rule modes_plot:
    input: "out.nc", "pressure_tend.ncl"
    output: "modes.png"
    run:
        import xray
        ds = xray.open_dataset("out.nc")

        plt.subplot(2,1,1)
        ds.th_modes.sel(thind=1).isel(time=-1).plot()
        plt.subplot(2,1,2)
        ds.th_modes.sel(thind=2).isel(time=-1).plot()

        plt.savefig(output[0])

rule f11plot:
    input: "f11"
    output: "plot.png"
    run:
        x=  np.loadtxt(input[0])
        plt.plot(x)
        plt.savefig(output[0])

rule f11:
    input: "fort.11"
    output: "f11"
    shell: "cat {input[0]} | tr -d '\n' > {output[0]}"

