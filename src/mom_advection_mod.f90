module momentum_advection_mod
  use state_mod, only: dp
  use interp_mod, only : sym4, weno5
  use scalar_advection_mod, only: weno5_2d_advection
  implicit none
contains
subroutine u_momentum_advection_2d_lf (ux, uz, b, fx, dx, dz, g, sx, sz, w)
    ! BEGIN ARGS
    real(dp), dimension(:,:) :: ux, uz, b, fx
    real(dp)  dz(:) !< specific volume base state
    real(dp) :: dx
    integer g ! ghost cells
    intent(in) :: ux, uz, dx, dz
    intent(inout) :: fx


    !! work arrays

    real(dp), parameter :: MAX_SPEED = 100d0
    ! Symmetric interpolated velocity
    real(dp) :: sx(:,:), sz(:,:)
    real(dp) :: w(:,:), px
    real(dp) :: pp(6), fp, fm, up, um

    ! END ARGS
    integer i,j,nx,nz

    nx = size(ux,1)
    nz = size(ux,2)


    !------------------------
    ! (*) horizontal advection
    !------------------------
    do j=1,size(ux,2)
       do i=2,size(ux,1)-3
          pp = (ux(i-2:i+3,j)**2/2.0d0  + MAX_SPEED * ux(i-2:i+3,j))/2d0
          fp = weno5(pp(1:5))

          pp = (ux(i-2:i+3,j)**2/2.0d0  - MAX_SPEED * ux(i-2:i+3,j))/2d0
          fm = weno5(pp(6:2:-1))

          sx(i,j) = fp + fm

       end do
    end do

    ! Flux difference
    do j=1,size(ux,2)
       do i=2,size(ux,1)-3
          fx(i,j) = fx(i,j) + (sx(i-1,j) - sx(i,j))/dx
       end do
    end do

    !------------------------
    ! (*) vertical advection
    !------------------------

    !! U-momentum advection
    do j=1,size(ux,2)
       do i=2,size(ux,1)-2
          ! interpolate ux
          sz(i,j) = sym4(uz(i-1:i+2,j))

       end do
    end do

    ! advection by w
    do j=3,nz-4
       do i=1,nx

          pp = ux(i,j-2:j+3)
          up = (sz(i,j) + dabs(sz(i,j)))/2.0d0
          um = (sz(i,j) - dabs(sz(i,j)))/2.0d0

          sx(i,j)  = weno5(pp(:5)) * up  &
               + weno5(pp(6:2:-1)) * um
       end do
    end do

    ! take flux difference
    do j=g+1,nz-g
       do i=2,nx
          fx(i,j) = fx(i,j) + (sx(i, j-1) - sx(i, j))/dz(j)
       end do
    end do


    !------------------------------
    ! Hydrostatic pressure gradient
    !------------------------------

    do j=g+1,size(ux,2)
      do i=1,size(ux,1)
        sz(i,j) = - (-b(i+2,j) +8d0 *b(i+1,j) - 8d0 * b(i,j) + b(i-1,j))/dx/12d0
      end do
    end do

    ! Vertical integral
    w(g,:) = 0.0_dp
    do j=g+1,size(ux,2)
      do i=1,size(ux,1)
        w(i,j) = w(i,j-1) + (sz(i,j) + sz(i,j-1))* (dz(j) + dz(j-1)) / 4.0d0
      end do
    end do


    fx = fx + w

  end subroutine u_momentum_advection_2d_lf

  subroutine u_momentum_advection_2d (ux, uz, b, fx, dx, dz, g, sx, sz, w)
    ! BEGIN ARGS
    real(dp), dimension(:,:) :: ux, uz, b, fx
    real(dp)  dz(:) !< specific volume base state
    real(dp) :: dx
    integer g ! ghost cells
    intent(in) :: ux, uz, dx, dz
    intent(inout) :: fx


    !! work arrays
    ! Symmetric interpolated velocity
    real(dp) :: sx(:,:), sz(:,:)
    real(dp) :: w(:,:), px

    ! END ARGS
    integer i,j

    !! U-momentum advection
    do j=1,size(ux,2)
       do i=2,size(ux,1)-2
          ! interpolate ux
          sx(i,j) = sym4(ux(i-1:i+2,j))
          sz(i,j) = sym4(uz(i-1:i+2,j))

       end do
    end do

    call weno5_2d_advection(sx, sz, ux, fx, dx, dz, g, w)

    !! Hydrostatic pressure gradient

    do j=g+1,size(ux,2)
      do i=1,size(ux,1)
        sz(i,j) = - (b(i+1,j) - b(i,j))/dx
      end do
    end do

    ! Vertical integral
    w(g,:) = 0.0_dp
    do j=g+1,size(ux,2)
      do i=1,size(ux,1)
        w(i,j) = w(i,j-1) + (sz(i,j) + sz(i,j-1))* (dz(j) + dz(j-1)) / 4.0d0
      end do
    end do


    fx = fx + w


  end subroutine u_momentum_advection_2d

end module momentum_advection_mod
