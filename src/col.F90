! Set to MMS_TEST 1 to test the dry dynamical core using MMS
! #define MMS_TEST 1
! #define MC_ON 1
module test_anelastic
  use iso_c_binding
  use physics_mod
  implicit none
  integer, parameter :: dp =kind(0.0d0)

  real(dp), allocatable :: dz(:)
contains

  subroutine init(nz) bind(c)
    integer(c_int) nz
    integer g

    ! vertical profiles
    real(dp), dimension(nz) :: s0,p0



    ! Work arrays
    real(dp), dimension(1, nz) :: f,df,sx,sz,w


    integer i,j
    g =0



    !
    ! Initialization
    !
    !! initialize grid and profiles
    if (allocated(dz)) deallocate(dz)
    allocate(dz(1:nz))
    dz = 0

    open(unit=41, file='t0.txt')
    open(unit=42, file='p0.txt')
    open(unit=43, file='pdiff.txt')

    read(41,*) s0(1:nz)
    read(42,*) p0(1:nz)
    read(43,*) dz(1:nz)


    close(41)
    close(42)
    close(43)


    !! Initialize parameterizations
    call init_modes(s0, p0, dz, nz, g)
    call init_params

    !
    ! End initialization
    !



  end subroutine init
  subroutine f(th, teb, q, &
       hc, hs, hd, teb_st,&
       th_modes,&
       th_tend, teb_tend, q_tend,&
       hc_tend, hs_tend, nz) bind(c)

    integer(c_int) :: nz
    real(c_double), intent(in), dimension(1,nz) :: th
    real(c_double), intent(in), dimension(1) :: teb, q, hs, hc
    real(c_double), intent(in), dimension(1) :: teb_st
    real(c_double), intent(out) :: hd(1)
    real(c_double), intent(out), dimension(1,2) :: th_modes
    real(c_double), intent(out) ::th_tend(1,nz),&
         teb_tend(1), q_tend(1), hs_tend(1), hc_tend(1)



    ! work
    real(c_double), dimension(1,2) :: u_modes
    real(dp) :: ux_tend(1,nz), ux(1,nz), qheating(1,nz)
    real(c_double) ::  dx
    integer g
    dx = 0d0
    g = 0

    call physics_tend(ux, th, &
         teb, q,&
         hc, hs, hd,&
         teb_st,&
         dx, dz,&
         u_modes, th_modes,&
         ux_tend, th_tend,&
         teb_tend, q_tend,&
         hc_tend, hs_tend, qheating, g)


  end subroutine f

end module test_anelastic
