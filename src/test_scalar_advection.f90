program test_scalar_advection
  use state_mod, only: dp 
  use scalar_advection_mod, only: weno5_2d_advection
  implicit none

  integer, parameter :: n =110, g=5
  real(dp), parameter :: dx = 1_dp/float(n-2*g) 
  real(dp), dimension(n, n) :: phi, phi1, ux, uz, f, f1,  df, x, z
  real(dp) dz(n)

  real(dp) :: t, dt, tend
  integer i,j

  ! Initialize grid, velocity, and scalar
  dz(:) = dx
  do j=1,n
     do i=1,n
        x(i, j) = (i-1-g) * dx
        z(i, j) = (j-1-g) * dx

        ux(i,j) = 1.0d0
        uz(i,j) = 1.0_dp
     end do
  end do

  phi = dexp(-(x-.5)**2 - (z-.5)**2)**20

  ! do j=1,100
  !    do i=1,100
  !       if (i < 50) then
  !           phi(i,j) = 1.0d0
  !        else
  !           phi(i,j) = 0.0d0
  !        end if
  !    end do
  ! end do

  ! Perform time stepping
  t = 0.0_dp
  ! tend = 0_dp
  tend = 1.00_dp
  dt = dx / 4.0_dp/2_dp

  write(1) phi(g+1:n-g, g+1:n-g)
  !! Begin time stepping
  do
     print *, t

     ! Predictor
     call apply_periodic_2d(phi, g)
     call weno5_2d_advection(ux, uz, phi, f, dx, dz, g, df)
     phi1 = phi  + dt * f



     ! Corrector
     call apply_periodic_2d(phi1, g)
     call weno5_2d_advection(ux, uz, phi1, f1, dx, dz, g, df)
     phi = phi + dt*(f+f1)/2.0d0

     t = t + dt
     if (t > tend) then
        exit
     end if

 end do !! End time stepping

 write(2) phi(g+1:n-g, g+1:n-g)

contains 
  subroutine apply_periodic_2d(phi, g)
    real(dp) :: phi(:,:)
    integer nx,ny,g,i,j
    nx = size(phi,1)
    ny = size(phi,2)


    do j=1,ny
       phi(1:g,j) = phi(nx-g-g+1:nx-g,j)
       phi(nx-g+1:nx,j) = phi(g+1:g+g,j)
    end do

    do i=1,nx
       phi(i, 1:g) = phi(i, ny-g-g+1:ny-g)
       phi(i,ny-g+1:ny) = phi(i,g+1:g+g)
    end do
  end subroutine apply_periodic_2d

end program test_scalar_advection
