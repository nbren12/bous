program read
  implicit none
  integer, parameter :: n = 14
  real(8) t0(n), p0(n)
  
  open(unit=41, file='t0.txt')
  open(unit=42, file='p0.txt')

  read(41,*) t0
  read(42,*) p0
  print *, t0
  print *, p0
  
end program read
