! #define _DEBUG 0
#define _ADVEC 1
module physics_mod
  use state_mod, only: dp
  implicit none
  public  :: physics_tend, init_modes, init_params, hsbar, hcbar

  private

  real(dp) hour, minute, day, km
  parameter(hour=3600.d0, minute=60.0d0, day=86400.0d0, km=1000.d0)

  real(dp) c, l, t


  real(dp) cp, gamma, omega, r, theta0, gr, beta, EQ, n2,&
       cd, u0, tau_d, alpha_bar, fs, fd, fc, deltas, deltac,&
       theta_ebs_m_theta_eb, theta_d, lcp

  real(dp) a,b,a0,a1,a2,a0p,a1p,a2p,alpha2,alpha3,&
       lambdas,alpha_s,alpha_c,xis,xic,mu,ud,thetap,thetam,zb,zt, &
       qr01,qr02,lambdabar,qcbar,dbar, theta_eb_m_theta_em,m0, &
       tau_conv,tau_e, tau_s,tau_c,tau_r, hsbar, hcbar, hdbar

  real(dp) gamma2p, skew_strat

  real(dp), parameter :: pi = 4*datan(1.d0),&
       TWO_SQRT2 = 2*dSQRT(2.d0),&
       sqrt2 = dsqrt(2d0)


  ! base state theta
  real(dp), allocatable :: th0(:)

  ! u-modes : z-centered
  real(dp), allocatable :: psi(:,:), apsi(:,:)

  ! w-modes : z-centered
  real(dp), allocatable :: phi(:,:), aphi(:,:)

  ! heating patterns : z-centered
  real(dp), allocatable :: phitr(:,:), phiC(:), phiS(:)

  ! average
  real(dp) :: phitrb(2), phib(2), phiCavg, phiSavg ! averaged

contains
  subroutine init_params()
    !     Parametrization parameters: a1=1; a2=0 ====> CAPE parametrization
    !                                 a1=0, a2=1=====> moisture parametrization

    a1=0.1D0
    a2=0.9D0
    a0=0.5d0

    a1p=1.d0
    a2p=0.0d0
    a0p=5.d0

    alpha2=.25D0 ! gamma_2 in the papers (JASI,JASII,TAO,TCFD)
    alpha3=.5D0 ! alpha_2 in the papers
    gamma2p = 6.0d0 !d


    !     lower threshold for parameter lambda: mesuring dryness and moisteness of middle troposphere


    lambdas=0.0D0

    !     Benchmark of standard parameters



    cp=1000.d0     !J/kg/K
    gamma=1.7D0   !none

    omega= 2*pi/(24*hour)! the angular velocity of the earth and
    r=6378*km           ! radius of the earth
    theta0=300.d0 !K background  reference temperature
    gr=9.8D0 !m/s gravitational acceleration
    beta=2*omega/r      !1/s/m
    EQ=40000*km!  Earth's peremeter at the Equator
    n2=.0001D0         !Vaissala buoyancy frequency squared


    zt=15.75*km !topospheric height
    zb=500.d0 !meters boundary layer depth
    cd=.001 ! momentum drag coefficient
    u0=2.d0 !m/s strength of turbulent fluctuations
    tau_d = 75*day! sec           Rayleigh-wind Relaxation Scale
    tau_r= 50*day !sec           Newtonian Cooling Relaxation Time Scale
    tau_s=3*hour  !sec           Stratiform adjustment time Scale (not to confuse with stratiform time scale which is given by tau_conv/al
    tau_conv=  2.0d0*hour !sec           Convective time scale
    ! tau_conv=  1.0*hour !sec           Convective time scale
    tau_c=1*hour   !sec   Congestus adjustment time scale (not to confuse with congestus time scale which is given by tau_conv/alpha_c )

    alpha_bar=(zt/pi)*n2*theta0/gr


    ! RCE fixed by QR01,theta_ebs_m_theta_eb,theta_eb_m_theta_em,thetap,thetam




    qr01 = 1.d0/day!/2/dsqrt(2.d0)       ! prescribed radiative cooling associated with first baroclinic
    ! qr01 = .01d0/day/phitrb(1)

    alpha_s=.25D0            ! ratio of deep convective and stratiform heating rates
    alpha_c=0.25d0  !ratio of deep convective and congestus time scales

    mu= 0.20D0   ! contribution of lower  tropospheric cooling (H_s - H_c) to downdrafts
    ! fs = 0.1D0 ! stratiform fraction  of total  surface rain fall.
    ! fd=0.8D0 !   deep convective fraction of total  surface rain fall.
    ! fc = 0.1D0 ! congestus fraction of total   surface precipitation


    ! xic= fc/fd/alpha_c
    ! xis = fs/fd/alpha_s
    skew_strat = .0
    xis = .4
    xic = 0.0d0

    xis = xis !* phitrb(2)/ phitrb(1)
    xic = xic !* phitrb(2)/ phitrb(1)



    deltas=(16-3*pi*xis)/(16+3*pi*xis)

    deltac=(16-3*pi*xic)/(16+3*pi*xic)

    PRINT*,'deltas=',deltas,'deltac=',deltac



    theta_ebs_m_theta_eb = 10.d0 !K discrepancy between boundary layer theta_e and its saturation value

    theta_eb_m_theta_em=12d0 !K discrepancy between boundary layer and middle tropospheric theta_e''s

    thetap=20.d0!
    thetam=10.d0 !K thresholds moistening and drying of middle troposphere

    a=(1.d0-lambdas)/(thetap-thetam)
    b=lambdas-a*thetam            !linear fit coefficients of LAMBDA

    !
    ! RCE calculation
    !


    ! OPEN(35,FILE="OUTPUT",STATUS='new')
    ! WRITE(35,*)'Evaporative time tau_e=', tau_e/3600/24,' days'

    ! PRINT*,'Evaporative time tau_e=', tau_e/3600/24,' days'
    ! WRITE(35,*) 'Type of RCE:'
    ! WRITE(35,*) '-------------'
    IF(theta_eb_m_theta_em <= thetam) THEN
       lambdabar=lambdas

       ! WRITE(35,*)'Pure deep convective RCE'
    ELSE IF(theta_eb_m_theta_em < thetap) THEN
       ! WRITE(35,*)'Mixed Deep convective-Congestus RCE'
       lambdabar=a*theta_eb_m_theta_em+b

    ELSE
       ! WRITE(35,*)'Pure Congestus RCE--dry troposphere'
       lambdabar=1.d0
    END IF

    ! WRITE(35,*)'********lambda_tilde=', lmd_tld


    qcbar=qr01*(1.d0-lambdas)/( (1.d0-lambdabar)*(1.d0+xis*alpha_s)  &
         + (lambdabar-lambdas)*xic*alpha_c) ! Bulk convective heating at RCE

    qcbar=qr01*(1.d0-lambdas)/(1.d0-lambdabar) 


    hdbar = (1.d0-lambdabar)/(1.d0-lambdas) * qcbar

    hsbar = alpha_s*(1.d0 -Lambdabar)*qcbar/(1.d0-Lambdas)
    hcbar = alpha_c*(Lambdabar-Lambdas)*qcbar/(1.d0-Lambdas)

    dbar = phitrb(1) * hdbar + xic *hcbar  * phiCavg &
         -xis * hsbar  * phiSavg
    dbar = dbar*zt

    ! dbar =  (qr01 + xis * hsbar + xic *hcbar) * phitrb(1)*zt

    !        radiative cooling associated with second baroclinic
    m0=dbar/theta_eb_m_theta_em/(1.d0 + mu*(hsbar-hcbar)/qcbar)

    tau_e= theta_ebs_m_theta_eb/(dbar/zb)! Evaporative time scale


    ! WRITE(35,*)'RCE values'
    ! WRITE(35,*)'QR01=', qr01*day, ' K/day Qc=', qcbar*day,' K/day,'  &
    !      ,'   D=',dbar, 'K m/s', 'QR02=', qr02*day,' K/day'

    ! WRITE(35,*)' theta_eb - theta_em', theta_eb_m_theta_em, 'K'
    ! PRINT*, 'theta_ebs_m_theta_eb', theta_ebs_m_theta_eb, ' K'


    ! WRITE(35,*)'-----------------Type of Parametrization'

    ! WRITE(35,*)'a1=',a1,' a2=', a2,' a0=',a0
    ! PRINT*, 'a1=',a1,' a2=', a2,' a0=',a0
    ! WRITE(35,*)'Lambda_bar=', lambdabar
    ! PRINT*,'Lambda_bar=', lambdabar
    ! WRITE(35,*)'mu=', mu
    ! PRINT*,'mu=', mu
    ! WRITE(35,*)'alpha_c=',alpha_c,' tau_c=', tau_c/3600,  &
    !      ' hours; tau_s=', tau_s/3600, ' hours;tau_conv=', tau_conv/3600,  &
    !      ' hours;'

    ! PRINT*,'alpha_c=',alpha_c,' tau_c=', tau_c/3600,  &
    !      ' hours; tau_s=', tau_s/3600, ' hours;tau_conv=', tau_conv/3600,  &
    !      ' hours;'




    !  Reference scales


    c=DSQRT(n2)*zt/pi! ~50 m/sec; gravity wave speed; velocity time scale
    l=DSQRT(c/beta)  !~ 1500 km, equatorial rossby deformation radius; length scale.
    t=l/c            !~ 8.33 hours, time scale
    theta_d=alpha_bar !~15 degree K; temperature scale

    ! WRITE(35,*)'L=',l,' meters,  T=',t,'sec,  C=',c,  &
    !      ' met./sec; theta_d=alpha_bar=',alpha_bar, 'Kelvin'


    lcp= (1000/700)**(287.4/1004)*(2.504E6)/1004/alpha_bar
    ! moisture: ratio of latent heat of vaporization and heat capacity
    !times (p/p_0)^{-.28), p=700 hPa, P_0=1000 normalized by alpha_bar unit of temperature.



    !    NON DIMENTIONALIZATION OF RCE VALUES AND PARAMETERS USED BEYOND THIS


    ! a =a *alpha_bar

    ! theta_eb_m_theta_em=theta_eb_m_theta_em/alpha_bar
    ! theta_ebs_m_theta_eb =  theta_ebs_m_theta_eb/alpha_bar

    ! thetap=thetap/alpha_bar
    ! thetam=thetam/alpha_bar

    ! qr01 = qr01*t/alpha_bar

    ! qr02 = qr02*t/alpha_bar




    ! ud = cd*u0*t/zb + t/tau_d

    ! zb=zb/l
    ! zt=zt/l

    ! tau_r=tau_r/t
    ! tau_s=tau_s/t
    ! tau_c=tau_c/t
    ! tau_conv = tau_conv/t
    ! tau_e=tau_e/t

    ! qcbar = qcbar*t/alpha_bar
    ! dbar =  (dbar/alpha_bar)/c
    ! mu=mu/qcbar

    ! m0 = m0/c

    ! PRINT*,'m0',m0, 'mu/Qc',mu
    ! WRITE(35,*)'m0',m0*c,'    m/s'
  end subroutine init_params

  subroutine init_modes (s0, p0, dlev, nz, g)
    integer g, nz
    real(dp) :: s0(1:), p0(1:), dlev(-g+1:) ! baseline

    !work arrays
    real(dp), parameter :: ptrunc1=250d2, ptrunc2 = 200d2, psplit=490d2
    integer i


    ! initialize modes
    if (allocated(psi)) deallocate(psi)
    if (allocated(phi)) deallocate(phi)
    if (allocated(apsi)) deallocate(apsi)
    if (allocated(aphi)) deallocate(aphi)
    if (allocated(phitr)) deallocate(phitr)
    if (allocated(th0)) deallocate(th0)

    allocate(psi(-g+1:nz+g,2))
    allocate(apsi(-g+1:nz+g,2))
    allocate(phi(-g+1:nz+g,2))
    allocate(aphi(-g+1:nz+g,2))
    allocate(phitr(-g+1:nz+g,2))

    ! allocate base line theta profile
    allocate(th0(-g+1:nz+g))
    th0 = 0d0
    th0(1:nz) = s0(1:nz)

    ! Ghost cells should be 0
    psi   = 0d0
    apsi  = 0d0
    phi   = 0d0
    aphi  = 0d0
    phitr = 0d0

    ! read from file outputed by init.py
    open(unit=41, file='modes.txt')
    do i=1,2
       read(41,*) psi(1:nz,i)
       read(41,*) apsi(1:nz,i)
       read(41,*) phi(1:nz,i)
       read(41,*) aphi(1:nz,i)
    end do
    close(41)


    print *, 'matmul between psi and apsi (should be I_2)'
    print *, matmul(transpose(apsi), psi)

    ! Create Psitrunc
    do i=1,nz
       print *, p0(i)
       if (p0(i) > ptrunc1) phitr(i,1) = phi(i,1)
       if (p0(i) > ptrunc2) phitr(i,2) = phi(i,2)
    end do

    ! Create skew symmetric heating
    if (allocated(phiC)) deallocate(phiC)
    if (allocated(phiS)) deallocate(phiS)
    allocate(phiC(-g+1:nz+g))
    allocate(phiS(-g+1:nz+g))
    phiC = (phitr(:,2) + dabs(phitr(:,2)))/2
    phiS = (phitr(:,2) - dabs(phitr(:,2)))/2

    phiCavg = dot_product(phiC, dlev)/sum(dlev)
    phiSavg = dot_product(phiS, dlev)/sum(dlev)

    print *, 'avg', phiCavg, phiSavg

    ! Take vertical average
    do i=1,2
       phitrb(i) = dot_product(phitr(:,i), dlev) /sum(dlev)
       phib(i) = dot_product(phi(:,i), dlev) /sum(dlev)
    end do
    print *, 'Vertical average is ', phitrb

  end subroutine init_modes

#ifdef _ADVEC
  subroutine moisture_advection(q, u1, u2, dx, q_tend)
    use scalar_advection_mod, only: weno5_1d_advection
    real(dp), dimension(1:) :: q, u1, u2, q_tend ! ghosted
    real(dp), intent(in) ::  dx
    intent(in) :: q, u1, u2
    intent(out) :: q_tend

    ! Parameters for moisture equation
    real(dp) q_tld,alpha_tld,lmd_tld
    parameter(alpha_tld=0.1d0,lmd_tld=76d0, q_tld=76d0)

    ! work arrays
    integer i
    real(dp), dimension(size(q)) :: eff_vel, df


    ! nonlinear advection by an effective_velocity
    eff_vel = u1 + alpha_tld * u2

    q_tend = 0d0 ! make sure q_tend is zeroed out
    call weno5_1d_advection(eff_vel, q, q_tend, dx, df)

    ! mean moisture tendency terms
    do i=2,size(q)
       q_tend(i) = q_tend(i) + q_tld* (u1(i-1)-u1(i))/dx + lmd_tld *(u2(i-1) - u2(i)) / dx
    end do

  end subroutine moisture_advection
#endif
  subroutine physics_tend(ux, th, teb, q, &
       hc, hs, hd, teb_st,&
       dx, dlev,&
       u_modes, th_modes,&
       ux_tend, th_tend, teb_tend, q_tend,&
       hc_tend, hs_tend,&
       qheating,&
       g)

    integer g
    real(dp), intent(in), dimension(:,-g+1:) :: ux, th
    real(dp), intent(in), dimension(:) :: teb, q, hs, hc
    real(dp), intent(in), dimension(:) :: teb_st
    real(dp), intent(in) :: dlev(:)
    real(dp), intent(in) :: dx
    real(dp), intent(out) :: hd(:)
    real(dp), intent(out), dimension(:,:) :: u_modes, th_modes
    real(dp), intent(out) :: ux_tend(:,-g+1:), th_tend(:,-g+1:),qheating(:,-g+1:),&
         teb_tend(:), q_tend(:), hs_tend(:), hc_tend(:)

    !work arrays
    integer i

    ! multicloud declarations
    real(dp), dimension(size(q)) :: ubar
    real(dp) :: lambda, tht_eb_tht_em
    real(dp) :: pr0, pr1, d, fthteb, fhc, fhs, fq, ftht1, ftht2, pr, fu1, fu2



    ux_tend = 0d0
    th_tend = 0d0
    teb_tend = 0d0
    q_tend = 0d0
    hc_tend = 0

    ! Compute velocity and temperature projections
    u_modes = matmul(ux, apsi)
    ubar = matmul(ux, dlev) ! should be zero
    th_modes = matmul(th, aphi)

    ! Do the moisture equation
#ifdef _ADVEC
    call moisture_advection(q, u_modes(:,1), u_modes(:,2), dx, q_tend)
#endif

    ! TEB advection

    !
    ! Multicloud Model Source terms (from km08)
    !


    do i=1,size(q,1)
        PR0 = qcbar + (A1*teb(I) + A2*Q(I) - &
        A0*(th_modes(i,1)+ ALPHA2*th_modes(i,2)))/TAU_CONV

        pr0 = dmax1(pr0,0.d0)

        PR1 = qcbar + (  A1p*teb(I) + A2p*Q(I) - &
        A0p*(th_modes(i,1)+ gamma2p*th_modes(i,2)))/TAU_CONV
        PR1=dmax1(PR1,0.d0)



        THT_EB_THT_EM = THETA_EB_M_THETA_EM + teb(I) &
        - phib(1) * (th_modes(i,1) + ALPHA3*th_modes(i,2))- Q(I)

        IF (THT_EB_THT_EM > THETAP) THEN
            LAMBDA=1.D0
        !            print*,'dry RCE. stoped'
        !            stop
        ELSEIF (THT_EB_THT_EM > THETAM)THEN
            LAMBDA = A* THT_EB_THT_EM + B
        !            print*,'mixed RCE. stoped'
        !            stop
        ELSE
            LAMBDA = LAMBDAS
        ENDIF

        hd(i) =(1.D0 - LAMBDA)/(1.D0-LAMBDAS)*PR0

    !         HC(I)=alpha_c *(LAMBDA-LAMBDAS)/(1.D0-LAMBDAS)*PR0

        D = DMAX1(0.D0,(1.D0 + MU*(hsbar-hcbar +HS(I)- HC(I))/qcbar))*m0 &
        *THT_EB_THT_EM


        PR = phitrb(1) * Hd(i) + xic * (hcbar + hc(i)) * phiCavg &
             -xis * (hsbar + hs(i)) * phiSavg
        ! PR= Hd(I) + xic*(hcbar + Hc(I)) + xis*(hsbar+ Hs (I))



        FU1 = - UD*u_modes(i,1)
        FU2 = - UD*u_modes(i,2)



    !         print*,'QR01', QR01, 'Hsbar=', hsbar,'QR02', QR02
    !        stop

        FTHT1 = PR - QR01 - th_modes(i,1)/TAU_R

        ! FTHT2 = (HC(I)-HS(I) +hcbar - hsbar) - th_modes(i,2)/TAU_R - qr02
        ! FTHT2 = (HC(I)-HS(I) +hcbar - hsbar) - th_modes(i,2)/TAU_R - qr02

        ! qheating(i,:) =  (HC(I)-HS(I) +hcbar-hsbar)  * phitr(:,2) + PR * phitr(:,1)
        ! Skew heating basis
        qheating(i,:) = phiC *(hc(i) - skew_strat * hs(i))&
             - phiS * hs(i)&
             + hd(i) * phitr(:,1)

        th_tend(i,:) = qheating(i,:) &
             - phitr(:,1) * (qr01 + th_modes(i,1)/tau_r) &
             - phitr(:,2) * th_modes(i,2)/tau_r

        teb_tend(i) = (theta_ebs_m_theta_eb + teb_st(I)- teb(I))/TAU_E - D/ZB

        q_tend(i) = q_tend(i) + D/ZT - PR
        hs_tend(i) = (ALPHA_S* Hd(I) - HS(I)-hsbar)/TAU_S


        hc_tend(i) = (ALPHA_C*(LAMBDA-LAMBDAS)/(1.d0-LAMBDAS)*PR1 &
        -HC(I)-hcbar)/TAU_C


#ifdef _DEBUG
           print *, 'teb-tem' ,tht_eb_tht_em
           print *, 'lambda' ,lambda
           print *, 'modes' ,th_modes(i,:)
           print *, 'hs', hsbar
           print *, 'hc', hcbar 
           print *, 'rad', -th_modes(i,2) /tau_r -qr02
           print *, 'pr', pr, qr01, xis, xic
           print *, 'ftht',ftht1,  ftht2
           print * ,ftht1,ftht2, teb_tend(i), q_tend(i), hs_tend(i), hc_tend(i)
#endif
    end do
    ! print * ,ftht1,ftht2, teb_tend(i), q_tend(i), hs_tend(i), hc_tend(i)



    ! ux_tend = -ud * ux

    ! ux = ux + ux_tend  * dt
    ! th = th + th_tend  * dt
    ! q  = q + q_tend  * dt
    ! teb  = teb + teb_tend  * dt
    ! hs  = hs + hs_tend  * dt
    ! hc  = hc + hc_tend  * dt

  end subroutine physics_tend

end module physics_mod
