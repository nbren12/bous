module io_mod
  use netcdf
  implicit none

  ! This is the name of the data file we will create.
  character (len = *), parameter :: FILE_NAME = "${name}"
  integer :: ncid

! Declare dimensions
  <%
  ndims = len(coords)
  %>

  integer, parameter :: NDIMS = ${ndims}, NRECS = 2

  % for dim,val in coords.items():
  character (len = *), parameter :: ${dim}_NAME = "${val['name']}"
  integer :: ${dim}_dimid

  % endfor

  integer, parameter :: NLVLS = 2, NLATS = 6, NLONS = 12

  ! The start and count arrays will tell the netCDF library where to
  ! write our data.
  integer :: start(NDIMS), count(NDIMS)

  ! These program variables hold the latitudes and longitudes.
  real :: lats(NLATS), lons(NLONS)
  integer :: lon_varid, lat_varid

  ! Create netcdf variables
  % for v,val in variables.items():
  character (len = *), parameter :: ${v}_name="${val['name']}"
  character (len = *), parameter :: ${v}_units = "${val['units']}"
  integer :: ${v}_varid

  % endfor

  integer :: dimids(NDIMS)

  character (len = *), parameter :: UNITS = "units"

contains
  subroutine init_netcdf_io (nx, nz)

    ! Loop indices
    integer :: lvl, lat, lon, rec, i

    ! Create the file. 
    call check( nf90_create(FILE_NAME, nf90_clobber, ncid) )

    ! Define the dimensions. The record dimension is defined to have
    ! unlimited length - it can grow as needed. In this example it is
    ! the time dimension.
    call check( nf90_def_dim(ncid, LVL_NAME, NLVLS, lvl_dimid) )
    call check( nf90_def_dim(ncid, LAT_NAME, NLATS, lat_dimid) )
    call check( nf90_def_dim(ncid, LON_NAME, NLONS, lon_dimid) )
    call check( nf90_def_dim(ncid, REC_NAME, NF90_UNLIMITED, rec_dimid) )

    ! Define the coordinate variables. We will only define coordinate
    ! variables for lat and lon.  Ordinarily we would need to provide
    ! an array of dimension IDs for each variable's dimensions, but
    ! since coordinate variables only have one dimension, we can
    ! simply provide the address of that dimension ID (lat_dimid) and
    ! similarly for (lon_dimid).
    call check( nf90_def_var(ncid, LAT_NAME, NF90_REAL, lat_dimid, lat_varid) )
    call check( nf90_def_var(ncid, LON_NAME, NF90_REAL, lon_dimid, lon_varid) )

    ! Assign units attributes to coordinate variables.
    call check( nf90_put_att(ncid, lat_varid, UNITS, LAT_UNITS) )
    call check( nf90_put_att(ncid, lon_varid, UNITS, LON_UNITS) )

    ! The dimids array is used to pass the dimids of the dimensions of
    ! the netCDF variables. Both of the netCDF variables we are creating
    ! share the same four dimensions. In Fortran, the unlimited
    ! dimension must come last on the list of dimids.
    dimids = (/ lon_dimid, lat_dimid, lvl_dimid, rec_dimid /)

    ! Define the netCDF variables for the pressure and temperature data.
    call check( nf90_def_var(ncid, PRES_NAME, NF90_REAL, dimids, pres_varid) )
    call check( nf90_def_var(ncid, TEMP_NAME, NF90_REAL, dimids, temp_varid) )

    ! Assign units attributes to the netCDF variables.
    call check( nf90_put_att(ncid, pres_varid, UNITS, PRES_UNITS) )
    call check( nf90_put_att(ncid, temp_varid, UNITS, TEMP_UNITS) )

    ! End define mode.
    call check( nf90_enddef(ncid) )

    ! Write the coordinate variable data. This will put the latitudes
    ! and longitudes of our data grid into the netCDF file.
    call check( nf90_put_var(ncid, lat_varid, lats) )
    call check( nf90_put_var(ncid, lon_varid, lons) )

    ! These settings tell netcdf to write one timestep of data. (The
    ! setting of start(4) inside the loop below tells netCDF which
    ! timestep to write.)
    count = (/ NLONS, NLATS, NLVLS, 1 /)
    start = (/ 1, 1, 1, 1 /)

    ! Write the pretend data. This will write our surface pressure and
    ! surface temperature data. The arrays only hold one timestep worth
    ! of data. We will just rewrite the same data for each timestep. In
    ! a real :: application, the data would change between timesteps.
    do rec = 1, NRECS
       start(4) = rec
       call check( nf90_put_var(ncid, pres_varid, pres_out, start = start, &
            count = count) )
       call check( nf90_put_var(ncid, temp_varid, temp_out, start = start, &
            count = count) )
    end do

    ! Close the file. This causes netCDF to flush all buffers and make
    ! sure your data are really written to disk.
    call check( nf90_close(ncid) )

    print *,"*** SUCCESS writing example file ", FILE_NAME, "!"
  end subroutine init_netcdf_io

  subroutine check(status)
    integer, intent ( in) :: status

    if(status /= nf90_noerr) then 
       print *, trim(nf90_strerror(status))
       stop "Stopped"
    end if
  end subroutine check

end module io_mod
