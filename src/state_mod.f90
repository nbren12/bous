module state_mod
  implicit none
  integer, parameter :: dp = kind(0.0d0)

  integer, parameter :: g=5, nx=250, nxg=nx+2*g, nz=28, nzg = nz + 2 *g

  type state_t

     ! grid
     real(dp), dimension(-g+1:nx+g, -g+1:nz+g) :: x, z, p
     real(dp), dimension(-g+1:nz+g) :: dz

     !
     ! State arrays
     !

     ! two dimensional

     ! ghosted the convention for staggered variables is index u_i is centered at
     ! x_{i+1/2}
     real(dp), dimension(-g+1:nx+g,-g+1:nz+g) :: ux, s
     real(dp), dimension(-g+1:nx+g,-g+1:nz+g) :: uz, b


     ! work arrays

     real(dp), dimension(-g+1:nx+g,-g+1:nz+g) :: ux1, s1


     ! one dimensional
     real(dp), dimension(-g+1:nx+g) :: teb, moist_cwv, teb_st, hs, hc, hd

     ! u_mode and t_modes
     real(dp), dimension(-g+1:nx+g,2) :: u_modes, th_modes

   contains
     procedure :: fill_ghost_cells


  end type state_t
  !
  ! Tendency arrays
  !
  real(dp), dimension(-g+1:nx+g,-g+1:nz+g) :: ux_tend, th_tend, qheating
  real(dp), dimension(-g+1:nx+g)    :: teb_tend, q_tend, hc_tend, hs_tend

contains


  subroutine fill_bc_z_rigidw(uz, g)
    ! The upper boundary is included in uz
    ! The lower is not
    real(dp) :: uz(:,:)
    integer nx,nz,g,i,j
    nx = size(uz,1)
    nz = size(uz,2)


    do i=1,nx
       ! Lower Boundary
       uz(i,g)  = 0_dp
       do j=1,g-1
          uz(i,g-j)= -uz(i, g+j)
       end do

       ! upper boundary
       uz(i,nz-g)  = 0_dp
       do j=1,g
          uz(i,nz-g+j)= -uz(i, nz-g-j)
       end do

    end do

  end subroutine fill_bc_z_rigidw

  subroutine fill_bc_z_dirichlet_scalar(phi, lowup, g)
    ! The upper boundary is included in uz
    real(dp) :: phi(:,:)
    real(dp) :: lowup(2)
    integer nx,nz,g,i,j
    nx = size(phi,1)
    nz = size(phi,2)


    do i=1,nx
       do j=1,g
          ! Lower Boundary
          phi(i,g+1-j)= lowup(1) -phi(i, g+j)

          ! upper boundary
          phi(i,nz-g+j)= lowup(2)-phi(i, nz-g+1-j)
       end do
    end do

  end subroutine fill_bc_z_dirichlet_scalar

  subroutine fill_bc_z_extrap_scalar(phi, g)
    ! The upper boundary is included in uz
    real(dp) :: phi(:,:)
    integer nx,nz,g,i,j
    nx = size(phi,1)
    nz = size(phi,2)


    do i=1,nx
       do j=1,g
          ! Lower Boundary
          phi(i,g+1-j)= phi(i, g+1)

          ! upper boundary
          phi(i,nz-g+j)= phi(i, nz-g)
       end do
    end do

  end subroutine fill_bc_z_extrap_scalar

  subroutine fill_bc_z_neumann_scalar(phi, g)
    ! The upper boundary is included in uz
    real(dp) :: phi(:,:)
    integer nx,nz,g,i,j
    nx = size(phi,1)
    nz = size(phi,2)


    do i=1,nx
       do j=1,g
          ! Lower Boundary
          phi(i,g+1-j)= phi(i, g+j)

          ! upper boundary
          phi(i,nz-g+j)= phi(i, nz-g+1-j)
       end do
    end do

  end subroutine fill_bc_z_neumann_scalar

  subroutine fill_bc_x_periodic(phi, g)
    real(dp) :: phi(:,:)
    integer nx,ny,g,i,j
    nx = size(phi,1)
    ny = size(phi,2)


    do j=1,ny
       phi(1:g,j) = phi(nx-g-g+1:nx-g,j)
       phi(nx-g+1:nx,j) = phi(g+1:g+g,j)
    end do

  end subroutine fill_bc_x_periodic

  subroutine periodic_1d(phi, g)
    real(dp) phi(:)
    integer g
    integer nx
    nx = size(phi, 1)

    phi(1:g) = phi(nx-g-g+1:nx-g)
    phi(nx-g+1:nx) = phi(g+1:g+g)
  end subroutine periodic_1d

  subroutine fill_ghost_cells(state)
    class(state_t) state

    ! Fill boundary conditions
    call fill_bc_z_extrap_scalar(state%s, g)
    call fill_bc_z_extrap_scalar(state%b, g)
    call fill_bc_z_extrap_scalar(state%ux, g)
    call fill_bc_z_rigidw(state%uz, g)

    call fill_bc_x_periodic(state%ux, g)
    call fill_bc_x_periodic(state%uz, g)
    call fill_bc_x_periodic(state%s, g)

    call periodic_1d(state%teb, g)
    call periodic_1d(state%moist_cwv, g)
    ! call periodic_1d(state%hs, g)
    ! call periodic_1d(state%hc, g)


  end subroutine fill_ghost_cells

end module state_mod
