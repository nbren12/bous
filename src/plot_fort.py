from pylab import *
from mpl_toolkits.axes_grid1 import AxesGrid
from scipy.io import FortranFile

n = 100
arr = FortranFile("fort.1").read_reals(dtype=np.float64).reshape((n,n))
arr_soln = FortranFile("fort.2").read_reals(dtype=np.float64).reshape((n,n))

F = plt.figure()
grid = AxesGrid(F, 111, # similar to subplot(111)
                nrows_ncols = (1, 3),
                axes_pad = 0.1,
                add_all=True,
                share_all=True,
                label_mode = "L",
                cbar_mode='each',
                cbar_location="top"
                )

im = grid[0].imshow(arr)
grid.cbar_axes[0].colorbar(im)

im = grid[1].imshow(arr_soln)
grid.cbar_axes[1].colorbar(im)

im = grid[2].imshow(arr-arr_soln)
grid.cbar_axes[2].colorbar(im)

show()
