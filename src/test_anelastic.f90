program test_anelastic
  use state_mod, only: dp 
  use scalar_advection_mod, only: weno5_2d_advection
  use momentum_advection_mod, only: u_momentum_advection_2d
  use diagnostics_mod
  use thermo_mod
  implicit none

  integer, parameter :: g=5, nx=200, nxg=nx+2*g, nz=32, nzg = nz + 2 *g
  real(dp), parameter :: km=1000_dp, hr=3600_dp, ler=1500*km, c=50.0, te=ler/c, N2=0.0001, &
       HT= 16*km, dx = 4000*km/nx, dz=km



  ! State arrays
  real(dp), dimension(-g+1:nx+g, -g+1:nz+g) :: ux, uz, s

  ! vertical profiles
  real(dp), dimension(-g+1:nz+g) :: T0, a0, p0, s0

  ! grid
  real(dp), dimension(-g+1:nx+g, -g+1:nz+g) :: x, z

  ! Vertical boundary conditions
  real(dp) sbc(2), ubc(2)

  ! Work arrays
  real(dp), dimension(nxg, nzg) :: b
  real(dp), dimension(nxg, nzg) :: ux1, uz1, s1
  real(dp), dimension(nxg, nzg) :: f,df,sx,sz,w

  ! Time stepping
  real(dp) :: t, dt, tend

  integer i,j

  !! Initialize 
  do j=-g+1,nz+g
     do i=1,nx
        x(i, j) = ( i-1 ) * dx
        z(i, j) = (j-1) * dz

        ux(i,j) = 0.0d0
        uz(i,j) = 0.0_dp

     end do
  end do

  !! Initialize base state

  T0 = 300 - z(10,:) * 4.5_dp/1d3
  T0(g+16:) = T0(g+16) ! constant temperature stratosphere




  call diagnose_base_profiles(T0, a0, p0, dz, g)
  s0 = theta(T0, p0)
  print *, 'Initial profile information:'
  print *, '----------------------------'
  print *, 'height', z(g+1,1:nz)
  print *, 'pressure', p0(1:nz)
  print *, 'temperature', T0(1:nz)
  print *, 'pot temp', s0(1:nz)
  print *, 'a0', a0(1:nz)
  print *, 'dx',dx/km, 'km'

  !! Intial conditions
  s = spread(s0,1,size(s,1))

  ! boundary conditions for entropy
  sbc(1) = theta(T0(1), p0(1))
  sbc(2) = theta(T0(nz), p0(nz))

  ubc = (/-50, 50/)


  ! Sheared case
  ! ux = 10 * dsqrt(2.0_dp) * dsin(z * 3.14/HT ) &
  !      + (-10)* dsqrt(2.0_dp) * dsin(2*z *3.14/HT )
  ux = 0.0_dp

  s = s +  .01*dexp(-((x-nx/2*dx)/(200*km))**2/.1_dp -  ( (z-5000)/(2000) )**2)

  ! Setup time stepping
  t = 0.0_dp
  ! tend = 0_dp
  tend = .1* hr
  dt = dx / 4.0 / 100


  ! diagnose the vertical velocity
  call diagnose_w(ux, a0, dx, dz, g, uz)

  write(1) s(1:nx,1:nz)
  !!! Begin time stepping
  do
     print *, t

     !! BEGIN CODE

     ! Copy into new arrays
     ux1 = ux
     s1  = s

     call anelastic_hsb_2d_step(ux, uz, s, a0, p0, dx, dz, dt, g, f,&
          df, sx, sz, w, b)
     call anelastic_hsb_2d_step(ux, uz, s, a0, p0, dx, dz, dt, g, f,&
          df, sx, sz, w, b)

     ux = (ux + ux1) /2.0d0
     s = (s+ s1)/2.0d0

     !! END CODE

     t = t + dt
     if (t > tend) then
        exit
     end if

 end do !!! End time stepping


 ! write(2) temperature_dry(s(1:nx, 1:nz), spread(p0, 1, nx))
 write(2) ux(1:nx,1:nz)
contains

  subroutine anelastic_hsb_2d_step(ux, uz, s,&
       a0, p0, &
       dx, dz, dt, g, f, &
       df, sx, sz, w, b)

    integer g, nx,nz
     real(dp), intent(inout), dimension(:,-g+1:) :: ux, uz, s, f
     real(dp), dimension(-g+1:) :: a0, p0 !< base state
     real(dp) dx, dz, dt


     ! Work arrays
     real(dp), dimension(:,-g+1:) :: sx, sz, w, df, b
     real(dp) bbc(2)

     bbc = (/0_dp, 0_dp/)


     nx = size(s,1)
     nz = size(a0,1)

     ! diagnose bouyancy
     do j=1,ubound(s, 2)
        do i=1,nx
           b(i,j) = vol_dry(s(i,j), p0(j))
           b(i,j) = 9.81_dp * (b(i,j) - a0(j))/a0(j)
        end do
     end do

     ! Fill boundary conditions
     call fill_bc_z_dirichlet_scalar(s, sbc, g)
     call fill_bc_z_dirichlet_scalar(s, bbc, g)
     call fill_bc_z_neumann_scalar(ux, g)
     call fill_bc_z_rigidw(uz, g)

     call fill_bc_x_periodic(ux, g)
     call fill_bc_x_periodic(uz, g)
     call fill_bc_x_periodic(s, g)
     call fill_bc_x_periodic(b, g)




     print *, 'bouyancy', b(100,10)
     print *, 'ux', ux(100,10)
     
     ! advection of entropy
     call weno5_2d_advection(ux, uz, a0, s, f, dx, dz, g, df)
     s = s + dt * f

     ! sponge layer

     ! advect velocity
     call u_momentum_advection_2d(ux, uz, b, a0, f, dx, dz, g, sx, sz, w)
     ux = ux + dt*f

     ! Pressure correction
     call pressure_correct_hsb_2d(ux, a0, g)

     ! diagnose the vertical velocity
     call diagnose_w(ux, a0, dx, dz, g, uz)

     ! sponge layer
     call sponge_layer(ux, s, s0, g)



     !! END CODE
   end subroutine anelastic_hsb_2d_step

   subroutine sponge_layer (ux, s, s0, g)
     integer g
     real(dp), dimension(-g+1:,-g+1:), intent(inout) :: ux, s
     real(dp) s0(:)

     ! Work
     ! Sponge layer 
     integer, parameter :: nsponge = 10
     real(dp) zsponge(nsponge)

     integer i, sstart, sstop

     sstart = ubound(s, 2)-g-nsponge
     sstop  = ubound(s, 2) - g

     zsponge = (/(i**2, i=1,nsponge)/) / 1/hr

     do i=i,nsponge
        ! relax velocity
        ux(:,sstart+i) = ux(:, sstart+i) * dexp(-zsponge(i)*dt)

        ! relax pottemp
        s(:,sstart+i) = (s(:, sstart+i) - s0(sstart+i)) * dexp(-zsponge(i)*dt) &
             + s0(sstart+i)
     end do


   end subroutine sponge_layer



 end program test_anelastic
