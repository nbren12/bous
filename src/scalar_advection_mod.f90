module scalar_advection_mod
  use interp_mod, only : weno5
  implicit none
  
contains
  !> Advect a scalar using high order finite differences
  !! @param a0(nz) base state specific volume
  !! @param g number of ghost cells
  subroutine weno5_2d_advection(ux, uz,  phi, f, dx, dz, g, df)
    ! Compute the tendency due to scalar advection
    ! df is a work array
    use state_mod, only : dp
    real(dp), dimension(:, :) :: ux, uz, phi, f,df
    real(dp), intent(in) ::  dz(:)
    real(dp) :: dx
    integer :: i, j, nx, nz
    intent(in) :: ux, uz, phi
    intent(out) :: f

    ! Work array
    real(dp) pp(6)

    integer g

    real(dp) :: um, up

    nx = size(ux,1)
    nz = size(ux,2)

    ! advection by u
    do j=1,nz
       do i=2,nx-3
          pp = phi(i-2:i+3,j)
          up = (ux(i,j) + dabs(ux(i,j)))/2.0d0
          um = (ux(i,j) - dabs(ux(i,j)))/2.0d0

          df(i,j)  = weno5(pp(:5)) * up  &
               + weno5(pp(6:2:-1)) * um
       end do
    end do


    ! take flux difference
    do j=2,nz
       do i=2,nx
          f(i,j) = (df(i-1,j) - df(i,j))/dx
       end do
    end do


    ! advection by w
    do j=3,nz-4
       do i=1,nx
          pp = phi(i,j-2:j+3)
          up = (uz(i,j) + dabs(uz(i,j)))/2.0d0
          um = (uz(i,j) - dabs(uz(i,j)))/2.0d0

          df(i,j)  = weno5(pp(:5)) * up  &
               + weno5(pp(6:2:-1)) * um 
       end do
    end do

    ! take flux difference
    do j=g+1,nz-g
       do i=2,nx
          f(i,j) = f(i,j) + (df(i, j-1) - df(i, j))/dz(j)
       end do
    end do


  end subroutine weno5_2d_advection

  subroutine weno5_1d_advection(ux, phi, f, dx, df)
    ! Compute the tendency due to scalar advection
    ! df is a work array
    use state_mod, only : dp
    real(dp), dimension(:) :: ux, phi, f,df
    real(dp) :: dx
    integer :: i, nx
    intent(in) :: ux, phi
    intent(out) :: f

    ! Work array
    real(dp) pp(6)


    real(dp) :: um, up

    nx = size(ux,1)

    ! advection by u
    do i=2,nx-3
       pp = phi(i-2:i+3)
       up = (ux(i) + dabs(ux(i)))/2.0d0
       um = (ux(i) - dabs(ux(i)))/2.0d0

       df(i)  = weno5(pp(:5)) * up  &
            + weno5(pp(6:2:-1)) * um
    end do


    ! take flux difference
    do i=2,nx
       f(i) = (df(i-1) - df(i))/dx
    end do


  end subroutine weno5_1d_advection
end module scalar_advection_mod
