sed -ibak 's/sin/dsin/g' inc.f90
sed -ibak 's/cos/dcos/g' inc.f90
sed -ibak 's/erf/derf/g' inc.f90
sed -ibak 's/cosh/dcosh/g' inc.f90
sed -ibak 's/sinh/dsinh/g' inc.f90
sed -ibak 's/tanh/dtanh/g' inc.f90
