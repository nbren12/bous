;********************************************
; h_long_5.ncl
;********************************************
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
;********************************************
begin
  f     = addfile ("anom.nc" , "r")
  th     = f->pert
  u     = f->ux


; ===========================
; color plot
; ===========================


; ===========================
; Hovmoller of deep heating
; ===========================

  wks   = gsn_open_wks ("pdf", "hov" )           ; open workstation
  gsn_define_colormap(wks,"BlWhRe")                ; choose colormap

  res = True
  res@gsnSpreadColors = True
  res@cnFillOn = True

  plot = gsn_csm_contour(wks, f->ux(:,10,:), res)

  delete(res)
  delete(plot)
  delete(wks)
  wks   = gsn_open_wks ("pdf", "th" )           ; open workstation
  ; gsn_define_colormap(wks_u,"BlWhRe")                ; choose colormap
  
  res_u                      = True                  ; plot mods desired
  ; res_u@tiMainString         = "After 6 Hours"        ; title

  ; res_u@cnLevelSelectionMode = "ManualLevels"        ; manual contour levels
  ; res_u@cnLevelSpacingF      = .2                   ; contour interval
  ; res_u@cnMinLevelValF       = -1.                  ; min level
  ; res_u@cnMaxLevelValF       =  2.                  ; max level
  res_u@cnLineLabelsOn       = True                  ; turn on line labels

  res_u@gsnContourNegLineDashPattern = 1
  res_u@cnLineLabelDensityF  = 2.0
  res_u@vpHeightF  = .3
  ; res_u@cnFillOn             = True                  ; turn on color fill

;---This resource not needed in V6.1.0
  ; res_u@gsnSpreadColors      = True                  ; use full range of colors

;---This resource defaults to True in NCL V6.1.0
  res_u@lbLabelAutoStride    = True                  ; optimal labels

  res_u@gsnDraw = False
  res_u@gsnFrame = False



  res_th = res_u
  res_th@cnFillPalette = "BlWhRe"
  res_th@cnFillOn = True
  res_th@cnLineLabelsOn = False
  ; res_th@cnLevelSelectionMode = "ManualLevels" ; manual contour levels
  ; res_th@cnMinLevelValF       = -2.5                ; set min contour level
  ; res_th@cnMaxLevelValF       =  2.5                ; set max contour level res_th@gsnSpreadColors = True
  ; res_th@cnLevelSpacingF      = .25                   ; contour interval


  symMinMaxPlt (th, 40, False,res_th)

; note this data is already on pressure levels. If this were model data,
; it would be necessary to interpolate the data from hybrid coordinates
; to pressure levels.

  ;; Line plot stuff
  res = True
  res@gsnDraw=False
  res@gsnFrame = False
  res@vpWidthF              = 0.70       ; change aspect ratio of plot
  res@vpHeightF             = 0.25
  i=0
  sz = dimsizes(u)


  res_th_tend = True
  res_th_tend@cnFillPalette        =  "BlWhRe"
  res_th_tend@vpHeightF =.3
  res_th_tend@gsnDraw = False
  res_th_tend@gsnFrame= False
  ; res_th_tend@gsnSpreadColors      = True                  ; use full range of colors
  res_th_tend@cnFillOn = True

  plot = new(4, graphic)


  do i=0,sz(0)-1
  ; do i=0,1

    tend = f->th_tend(i,:,:)*3600*34
    copy_VarCoords(f->th_tend(i,:,:), tend)
    symMinMaxPlt(tend, 20, False, res_th_tend)
    

    ;plot_u = gsn_csm_pres_hgt(wks,u(i,:,:),res_u)
    plot(0) = gsn_csm_pres_hgt(wks,th(i,:,:),res_th)
    plot(1) = gsn_csm_pres_hgt(wks,tend,res_th_tend)


    ;overlay(plot_th,plot_u)
    ; draw(plot_th)
    ; frame(wks)


    plot(2) = gsn_csm_xy(wks, f->x, f->hd(i,:)*3600*24, res)
    ;th1 = gsn_csm_xy(wks, f->x, f->th_modes(i,0,:), res)
    ;th2 = gsn_csm_xy(wks, f->x, f->th_modes(i,1,:), res)
    teb = gsn_csm_xy(wks, f->x, f->teb(i,:), res)
    q = gsn_csm_xy(wks, f->x, f->moist(i,:), res)

    plot(3) = teb
    overlay(teb,q)

    gsn_panel(wks, plot, (/4,1/), False)
    ; draw(th1)
    ; frame(wks)
  end do
  

  ; res@cnLevelSpacingF      = .1                   ; contour interval
  ; res@cnMinLevelValF       = -1.                  ; min level
  ; plot = gsn_csm_pres_hgt(wks,u(1,:,:),res)
  
; the "{}" allows you to use coordinate subscripting to select a particular 
; latitude vice array indice of the latitude you want. 
end

